﻿using UnityEngine;

namespace KUtils
{
	/// <summary>
	/// Extra math utility methods.
	/// </summary>
	public static class KMathf
	{
		/// <summary>
		/// Returns the distance between the given point and the line defined by origin and direction.
		/// </summary>
		public static float getPointToLineDistance (Vector3 point, Vector3 origin, Vector3 direction)
		{
			return Vector3.Cross(point - origin, direction).magnitude / direction.magnitude;
		}

		/// <summary>
		/// Returns the distance between the given point and the line defined by origin and direction.
		/// </summary>
		public static float getPointToLineDistance (Vector2 point, Vector2 origin, Vector2 direction)
		{
			return ((point.x - origin.x) * direction.y - (point.y - origin.y) * direction.x) / direction.magnitude;
		}

		/// <summary>
		/// Returns the distance between the given point and the line defined by origin and direction. Depending on the side the distance will be positive or negative.
		/// </summary>
		public static float getSignedPointToLineDistance (Vector2 point, Vector2 origin, Vector2 direction)
		{
			return ((point.x - origin.x) * direction.y - (point.y - origin.y) * direction.x) / direction.magnitude;
		}

		/// <summary>
		/// Returns the component value of the given vector onto the given axis.
		/// </summary>
		public static float getVectorComponent(Vector2 vector, Vector2 axis)
		{
			Vector2 projection = Vector3.Project(vector, axis);
			return projection.magnitude * (Vector2.Angle(projection, axis) > 0 ? -1.0F : 1.0F);
		}

		/// <summary>
		/// Returns a perpendicular normalized vector to the left.
		/// </summary>
		public static Vector2 getNormal (Vector2 vector)
		{
			return getNormalDirection(vector).normalized;
		}

		/// <summary>
		/// Returns a perpendicular normalized vector for the given segement (perpendicular to the left looking from start to end).
		/// </summary>
		public static Vector2 getNormal (Vector2 start, Vector2 end)
		{
			return getNormalDirection(start, end).normalized;
		}

		/// <summary>
		/// Returns a perpendicular vector (not normalized) to the left.
		/// </summary>
		public static Vector2 getNormalDirection (Vector2 vector)
		{
			return Vector3.Cross(vector, Vector3.back);
		}

		/// <summary>
		///  Returns a perpendicular vector (not normalized) for the given segement (perpendicular to the left looking from start to end).
		/// </summary>
		public static Vector2 getNormalDirection (Vector2 start, Vector2 end)
		{
			return getNormalDirection(end - start);
		}

		/// <summary>
		/// Checks if the given vec is between vectors from and to in a counter-clock wise rotation.
		/// </summary>
		public static bool isVectorBetween (Vector2 vec, Vector2 from, Vector2 to, bool inclusive = true)
		{
			float angle = getFullAngle(vec);
			float fromAngle = getFullAngle(from);
			float toAngle = getFullAngle(to);

			if (angle < fromAngle)
				angle += 2 * Mathf.PI;
			if (toAngle <= fromAngle)
				toAngle += 2 * Mathf.PI;

			if (inclusive)
				return angle >= fromAngle && angle <= toAngle;
			else
				return angle > fromAngle && angle < toAngle;
		}

		/// <summary>
		/// Checks if the given angle (in radians) is between from and to (in a counter-clock wise rotation).
		/// </summary>
		public static bool isAngleBetween (float angle, float from, float to, bool inclusive = true)
		{
			angle %= 2 * Mathf.PI; from %= 2 * Mathf.PI; to %= 2 * Mathf.PI;

			if (angle < 0) angle += 2 * Mathf.PI;
			if (from < 0) from += 2 * Mathf.PI;
			if (to < 0) to += 2 * Mathf.PI;

			if (angle < from)
				angle += 2 * Mathf.PI;
			if (to <= from)
				to += 2 * Mathf.PI;

			if (inclusive)
				return angle >= from && angle <= to;
			else
				return angle > from && angle < to;
		}

		/// <summary>
		/// Checks if the given angle (in degrees) is between from and to (in a counter-clock wise rotation).
		/// </summary>
		public static bool isAngleBetweenDeg (float angle, float from, float to, bool inclusive = true)
		{
			angle %= 360; from %= 360; to %= 360;

			if (angle < 0) angle += 360;
			if (from < 0) from += 360;
			if (to < 0) to += 360;

			if (angle < from)
				angle += 360;
			if (to <= from)
				to += 360;

			if (inclusive)
				return angle >= from && angle <= to;
			else
				return angle > from && angle < to;
		}

		/// <summary>
		/// Returns the angle (in radians) between right (1, 0, 0) and the given vector. Returned angle is in the range of [0, 2 * PI) radians.
		/// 270 degrees would be the equivalent to a 90 degrees clock wise rotation. Returns float.NaN if vector is zero.
		/// </summary>
		public static float getFullAngle (Vector2 vector)
		{
			// en el caso particular cuando (y / x) es infinito
			if (vector.x == 0)
			{
				if (vector.y > 0)
					return Mathf.PI / 2.0f;
				else if (vector.y < 0)
					return (3.0f * Mathf.PI) / 2.0f;
				else
					return float.NaN; // cuando el vector es cero
			}

			float angle = Mathf.Atan(vector.y / vector.x);

			if (vector.x < 0)
				angle += Mathf.PI;
			else if (vector.y < 0)
				angle += 2.0f * Mathf.PI;

			return angle;
		}

		/// <summary>
		/// Returns the angle (in degrees) between right (1, 0, 0) and the given vector. Returned angle is in the range of [0, 360) degrees.
		/// 270 degrees would be the equivalent to a 90 degrees clock wise rotation. Returns float.NaN if vector is zero.
		/// </summary>
		public static float getFullAngleDeg (Vector2 vector)
		{
			return getFullAngle(vector) * Mathf.Rad2Deg;
		}

		/// <summary>
		/// Same as UnityEngine.Mathf.SmoothDampAngle except that you can specify a min speed.
		/// </summary>
		public static float smoothDampAngle (float current, float target, ref float currentVelocity, float smoothTime, float minSpeed, float maxSpeed, float deltaTime)
		{
			float deltaAngle = getDeltaAngle(current, target);
			currentVelocity = ClampAbs(deltaAngle / smoothTime, minSpeed, maxSpeed);
			float clampedSmoothTime = deltaAngle / currentVelocity;

			return current + currentVelocity * Mathf.Min(deltaTime, clampedSmoothTime);
		}

		/// <summary>
		/// Clamps the absolute value of value.
		/// </summary>
		public static float ClampAbs (float value, float absMin, float absMax)
		{
			if (value < 0)
			{
				if (-value < absMin)
					return -absMin;
				else if (-value > absMax)
					return -absMax;
				else
					return value;
			}
			else
			{
				if (value < absMin)
					return absMin;
				else if (value > absMax)
					return absMax;
				else
					return value;
			}
		}

		/// <summary>
		/// Returns shortest angle you should rotate current to achieve target.
		/// </summary>
		public static float getDeltaAngle(float current, float target)
		{
			if ((current %= 360.0F) < 0) current += 360.0F;
			if ((target %= 360.0F) < 0) target += 360.0F;

			float delta0 = target - current;
			float delta1 = target - current - 360.0F * Mathf.Sign(delta0);
			float absDelta0 = Mathf.Abs(delta0);
			float absDelta1 = Mathf.Abs(delta1);

			if (absDelta0 < absDelta1)
				return delta0;
			else if (absDelta0 > absDelta1)
				return delta1;
			else
				return absDelta0;
		}

		/// <summary>
		/// Rounds the given number by reference. A round precision can also be specified (must be a power of ten).
		/// </summary>
		public static void roundTo (ref float number, float precision = 1.0F)
		{
			number /= precision;
			number = Mathf.Round(number);
			number *= precision;
		}

		/// <summary>
		/// Rounds the given number. A round precision can also be specified (must be a power of ten).
		/// </summary>
		public static float roundTo (float number, float precision)
		{
			roundTo(ref number, precision);
			return number;
		}

		/// <summary>
		/// Rounds the given vector by reference. A round precision can also be specified (must be a power of ten).
		/// </summary>
		public static void roundTo (ref Vector2 vector, float precision)
		{
			roundTo(ref vector.x, precision);
			roundTo(ref vector.y, precision);
		}

		/// <summary>
		/// Rounds the given vector. A round precision can also be specified (must be a power of ten).
		/// </summary>
		public static Vector2 roundTo (Vector2 vector, float precision)
		{
			roundTo(ref vector, precision);
			return vector;
		}

		/// <summary>
		/// Rounds the given vector by reference. A round precision can also be specified (must be a power of ten).
		/// </summary>
		public static void roundTo (ref Vector3 vector, float precision)
		{
			roundTo(ref vector.x, precision);
			roundTo(ref vector.y, precision);
			roundTo(ref vector.z, precision);
		}

		/// <summary>
		/// Rounds the given vector. A round precision can also be specified (must be a power of ten).
		/// </summary>
		public static Vector3 roundTo (Vector3 vector, float precision)
		{
			roundTo(ref vector, precision);
			return vector;
		}

		/// <summary>
		/// Rounds the given value to the closest multiple of snap.
		/// </summary>
		public static float snapValue (float value, float snap)
		{
			return Mathf.Round(value / snap) * snap;
		}

		/// <summary>
		/// Rounds all the vector fields to the closest multiple of snap.
		/// </summary>
		public static Vector3 snapVector (Vector3 vector, float snap)
		{
			vector.x = snapValue(vector.x, snap);
			vector.y = snapValue(vector.y, snap);
			vector.z = snapValue(vector.z, snap);

			return vector;
		}

		/// <summary>
		/// Checks the intersection between the sphere defined by center and radius and the line defined by linePoint and lineDirection.
		/// Intersection points are output in order relative to the lineDirection.
		/// If the line is tangent with the sphere both points will be equal.
		/// If the line doesn't intersect with the sphere both points will be (NaN, NaN, NaN).
		/// http://www.codeproject.com/Articles/19799/Simple-Ray-Tracing-in-C-Part-II-Triangles-Intersec
		/// </summary>
		public static void sphereWithLineIntersection (Vector3 center, float radius, Vector3 linePoint, Vector3 lineDirection, out Vector3 point0, out Vector3 point1)
		{
			float A = lineDirection.x * lineDirection.x + lineDirection.y * lineDirection.y + lineDirection.z * lineDirection.z;

			float B = 2.0F * (linePoint.x * lineDirection.x + linePoint.y * lineDirection.y + linePoint.z * lineDirection.z -
				lineDirection.x * center.x - lineDirection.y * center.y - lineDirection.z * center.z);

			float C = linePoint.x * linePoint.x - 2 * linePoint.x * center.x + center.x * center.x + linePoint.y * linePoint.y -
				2 * linePoint.y * center.y + center.y * center.y + linePoint.z * linePoint.z -
				2 * linePoint.z * center.z + center.z * center.z - radius * radius;

			// discriminant
			float D = B * B - 4.0F * A * C;

			if (D < 0.0F)
			{
				point0 = new Vector3(float.NaN, float.NaN, float.NaN);
				point1 = new Vector3(float.NaN, float.NaN, float.NaN);
			}
			else
			{
				float t1 = (-B - Mathf.Sqrt(D)) / (2.0F * A);
				point0.x = linePoint.x * (1.0F - t1) + t1 * (linePoint.x + lineDirection.x);
				point0.y = linePoint.y * (1.0F - t1) + t1 * (linePoint.y + lineDirection.y);
				point0.z = linePoint.z * (1.0F - t1) + t1 * (linePoint.z + lineDirection.z);

				if (D != 0.0F)
				{
					float t2 = (-B + Mathf.Sqrt(D)) / (2.0F * A);
					point1.x = linePoint.x * (1.0F - t2) + t2 * (linePoint.x + lineDirection.x);
					point1.y = linePoint.y * (1.0F - t2) + t2 * (linePoint.y + lineDirection.y);
					point1.z = linePoint.z * (1.0F - t2) + t2 * (linePoint.z + lineDirection.z);
				}
				else
					point1 = new Vector3(float.NaN, float.NaN, float.NaN);
			}
		}

		/// <summary>
		/// Checks the intersection between the sphere defined by center and radius and the line defined by linePoint and lineDirection.
		/// Returns the first intersection point relative to the lineDirection.
		/// If the line doesn't intersect with the sphere the point will be (NaN, NaN, NaN).
		/// </summary>
		public static Vector3 firstSphereWithLineIntersection (Vector3 center, float radius, Vector3 linePoint, Vector3 lineDirection)
		{
			Vector3 point0, point1;
			sphereWithLineIntersection(center, radius, linePoint, lineDirection, out point0, out point1);
			return point0;
		}

		/// <summary>
		/// Checks the intersection between the sphere defined by center and radius and the line defined by linePoint and lineDirection.
		/// Returns the second intersection point relative to the lineDirection.
		/// If the line doesn't intersect with the sphere the point will be (NaN, NaN, NaN).
		/// </summary>
		public static Vector3 secondSphereWithLineIntersection (Vector3 center, float radius, Vector3 linePoint, Vector3 lineDirection)
		{
			Vector3 point0, point1;
			sphereWithLineIntersection(center, radius, linePoint, lineDirection, out point0, out point1);
			return point1;
		}

		public static Matrix4x4 MatrixLerp (Matrix4x4 from, Matrix4x4 to, float time)
		{
			Matrix4x4 lerp = new Matrix4x4();

			for (int i = 0; i < 16; i++)
				lerp[i] = Mathf.Lerp(from[i], to[i], time);
			
			return lerp;
		}
	}
}

// el anterior algoritmo de colision entre linea y esfera que desarrolle (me apetecia guardarlo)
// el algoritmo utilizado actualmente lo saque de stackoverflow x)
//public static void sphereWithLineIntersection(Vector3 center, float radius, Vector3 linePoint, Vector3 lineDirection, out Vector3 point0, out Vector3 point1)
//{
//	// transformamos el punto de la linea en relacion al centro de la esfera
//	linePoint -= center;

//	// obtenemos los parametros para transformar los vectores de forma que queden sobre el plano XY cortando a la esfera en una seccion circular
//	float yAngle, z;

//	if (lineDirection.x == 0 && lineDirection.z == 0)
//	{ // este es el caso particular de que la linea sea un punto sobre el plano XZ (una linea vertical)
//		yAngle = 0.0F;
//		z = linePoint.z;
//	}
//	else
//	{
//		yAngle = Mathf.Atan2(lineDirection.z, lineDirection.x) * Mathf.Rad2Deg;
//		z = ((-linePoint.x * lineDirection.z) - (-linePoint.z * lineDirection.x)) / Mathf.Sqrt(lineDirection.x * lineDirection.x + lineDirection.z * lineDirection.z);
//	}

//	// el radio de la seccion circular cortada por el plano vertical que contiene a la linea
//	float sectionRadius = Mathf.Sqrt(radius * radius - z * z);

//	// transformamos los vectores para operar solo en XY
//	Quaternion yRotation = Quaternion.Euler(0.0F, yAngle, 0.0F);
//	linePoint = yRotation * linePoint;
//	lineDirection = yRotation * lineDirection;

//	// obtenemos la interseccion entre la linea y la seccion circular en el plano XY
//	float zAngle = Mathf.Atan2(lineDirection.y, lineDirection.x) * Mathf.Rad2Deg;
//	float y = ((-linePoint.x * lineDirection.y) - (-linePoint.y * lineDirection.x)) / Mathf.Sqrt(lineDirection.x * lineDirection.x + lineDirection.y * lineDirection.y);
//	float x = Mathf.Sqrt(sectionRadius * sectionRadius - y * y);

//	// devolvemos el punto obtenido realizando la transformacion adecuada de reversion
//	point0 = Quaternion.Euler(0.0F, -yAngle, zAngle) * new Vector3(-x, y, z) + center;
//	point1 = Quaternion.Euler(0.0F, -yAngle, zAngle) * new Vector3(x, y, z) + center;
//}