﻿using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
	public float sensitivity = 1;
	public float speed1;
	public float speed2;

	private Vector3 rotation;

	void Start ()
	{
		rotation = transform.rotation.eulerAngles;
	}
	
	void Update ()
	{
		if (Input.GetMouseButtonDown(0))
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
		else if (Input.GetMouseButtonUp(0))
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}

		if (Input.GetMouseButton(0))
		{
			// calculamos la rotacion que aplicaremos a la camara segun el movimiento del aton
			Vector3 deltaRotation = new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X")) * sensitivity;
			// aplicamos la rotacion calculada
			rotation += deltaRotation;
			// ajustamos l rotation dentro de unos limites
			rotation.x = Mathf.Clamp(rotation.x, -90, 90);
			rotation.y -= rotation.y > 360 ? 360 : 0;
			rotation.y += rotation.y < -360 ? 360 : 0;

			transform.rotation = Quaternion.Euler(rotation);
		}

		Vector3 position = new Vector3();

		if (Input.GetKey(KeyCode.W) || Input.GetKeyDown(KeyCode.W))
			position += Vector3.forward;
		if (Input.GetKey(KeyCode.S) || Input.GetKeyDown(KeyCode.S))
			position += Vector3.back;
		if (Input.GetKey(KeyCode.A) || Input.GetKeyDown(KeyCode.A))
			position += Vector3.left;
		if (Input.GetKey(KeyCode.D) || Input.GetKeyDown(KeyCode.D))
			position += Vector3.right;

		float speed = Input.GetKey(KeyCode.LeftShift) ? speed2 : speed1;
		position = transform.position + transform.rotation * (position * speed * Time.deltaTime);
		transform.position = position;
	}
}