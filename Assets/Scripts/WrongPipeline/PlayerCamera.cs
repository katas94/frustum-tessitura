﻿using UnityEngine;
using System.Collections;

namespace WrongPipeline
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Camera))]
	public class PlayerCamera : MonoBehaviour
	{
		public Player player;
		public Transform lookAt;
		public Transform target;
		public float distance = 5.0F;
		public float positionSmoothTime = 0.3F;
		public float rotationSmoothTime = 0.05F;
		public float planetChangeSmoothTime = 0.8F;
		public float sensitivity = 1;

		[HideInInspector]
		public bool planetChanged = false;

		private Vector3 rotation;
		private Vector3 offsetPosition;
		private int ignoreLayers;

		public void SetOnPlayer ()
		{
			rotation.z = 0.0F;
			
			rotation.x = 70; rotation.y = 0;
			offsetPosition = Quaternion.Euler(70, 0, 0) * Vector3.back * distance;
			target.position = lookAt.position + player.transform.TransformVector(offsetPosition);
			target.LookAt(lookAt, player.transform.up);

			if (gameObject.activeInHierarchy)
			{
				StopAllCoroutines();
				StartCoroutine(C_FollowTarget());
			}
		}

		public void SetChatched ()
		{
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
			catched = true;
		}

		void OnEnable()
		{
			StopAllCoroutines();
			StartCoroutine(C_FollowTarget());
		}

		void Start ()
		{
			ignoreLayers = ~(LayerMask.NameToLayer("Player") | LayerMask.NameToLayer("PlanetFields"));
			SetOnPlayer();
		}

		private bool catched = false;

		void Update ()
		{
			// calculamos la rotacion que aplicaremos a la camara segun el movimiento del aton
			Vector3 deltaRotation = new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X")) * sensitivity;
			
			if (!catched)
			{
				deltaRotation = Vector3.zero;
			}

			if (rotation.x + deltaRotation.x < -89 || rotation.x + deltaRotation.x > 89)
				deltaRotation.x = 0.0F;
			
			// aplicamos la rotacion calculada
			rotation += deltaRotation;
			// ajustamos l rotation dentro de unos limites
			// rotation.x = Mathf.Clamp(rotation.x, -89, 89);
			rotation.y -= rotation.y > 360 ? 360 : 0;
			rotation.y += rotation.y < -360 ? 360 : 0;

			// offsetPosition = Quaternion.Euler(rotation) * (Vector3.back * distance);
			// offsetPosition = player.transform.TransformVector(offsetPosition);
			
			player.transform.rotation = player.transform.rotation * Quaternion.Euler(0, deltaRotation.y, 0);
			deltaRotation.y = 0.0F;
			offsetPosition = Quaternion.Euler(deltaRotation) * offsetPosition;

			// player.cameraRotation = Quaternion.Euler(0, deltaRotation.y, 0) * player.cameraRotation;
			// player.cameraRotation = Quaternion.FromToRotation(player.transform.forward, Vector3.ProjectOnPlane(-offsetPosition, player.transform.up));

			RaycastHit hit;
			Vector3 direction = player.transform.TransformVector(offsetPosition);

			if (Physics.Raycast(lookAt.position, direction, out hit, distance, ignoreLayers) && !hit.collider.isTrigger && hit.collider.GetComponent<MeshRenderer>() != null)
				direction = direction.normalized * (hit.distance - 0.5F);

			target.position = lookAt.position + direction;
			target.LookAt(lookAt, player.transform.up);

			if (Input.GetMouseButtonDown(0))
			{
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Locked;
				catched = true;
			}
			if (catched && Input.GetKeyDown(KeyCode.Escape))
			{
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
				catched = false;
			}
		}

		private IEnumerator C_FollowTarget ()
		{
			Vector3 currentVelocity = Vector3.zero;

			Vector3 lookAtVelocity = Vector3.zero;
			Vector3 upVelocity = Vector3.zero;
			Vector3 currentForward = transform.forward;
			Vector3 currentUp = transform.up;

			while (true)
			{
				float smoothTime = planetChanged ? planetChangeSmoothTime : rotationSmoothTime;

				transform.position = Vector3.SmoothDamp(transform.position, target.position, ref currentVelocity, positionSmoothTime);
				currentForward= Vector3.SmoothDamp(currentForward, target.forward, ref lookAtVelocity, smoothTime);
				currentUp= Vector3.SmoothDamp(currentUp, target.up, ref upVelocity, smoothTime);
				transform.LookAt(transform.position + currentForward, currentUp);
				yield return null;
			}
		}

		private Coroutine planetChangedC;

		public void PlanetChanged ()
		{
			if (gameObject.activeInHierarchy)
			{
				if (planetChangedC != null)
					StopCoroutine(planetChangedC);

				planetChangedC = StartCoroutine(C_PlanetChange());
			}
		}

		private IEnumerator C_PlanetChange ()
		{
			planetChanged = true;
			yield return new WaitWhile(() => Vector3.Angle(transform.up, target.up) > 0.05F);
			planetChanged = false;
		}
	}
}