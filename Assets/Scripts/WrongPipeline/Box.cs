using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace WrongPipeline
{
	public class Box : MonoBehaviour
	{
		public AnimationCurve floatCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
		public float minScale = 1.0F;
		public float maxScale = 1.0F;
		public float offset = -2.0F;
		public float floatSpeed = 90.0F;
		public float floatHeight = 1.0F;

		public AudioSource fast, slow, exp;

		[HideInInspector]
		public bool isOrtho = false;

		private ParticleSystem explosion;

		void Awake()
		{
			explosion = GetComponent<ParticleSystem>();
		}

		void Start()
		{
			StartCoroutine(C_Float());
		}

		private IEnumerator C_Float ()
		{
			float rot = Random.Range(0.0F, 360.0F);
			Vector3 position = transform.position;

			while (true)
			{
				// Quaternion rotation = Random.rotation;
				// Quaternion local = transform.localRotation;

				// float time = 0.0F;

				// while (time < 2.0F)
				// {
				// 	transform.localRotation = Quaternion.Lerp(local, rotation, floatCurve.Evaluate(time / 2.0F));
				// 	time += Time.deltaTime;
				// 	yield return null;
				// }

				rot += floatSpeed  * Time.deltaTime;
				transform.position = position + transform.up * (floatHeight * Mathf.Sin(rot * Mathf.Deg2Rad));

				yield return null;
			}
		}

		public void Fire ()
		{
			if (isOrtho)
				WorldManager.orthoBoxes--;
			else
				WorldManager.persBoxes--;

			StartCoroutine(C_Destroy());
		}

		private IEnumerator C_Destroy ()
		{
			GetComponentInChildren<Collider>().enabled = false;
			exp.Play();

			if (!isOrtho)
				slow.Play();
			else
				fast.Play();
			
			GetComponentInChildren<MeshRenderer>().enabled = false;
			explosion.Play();
			yield return new WaitWhile(() => { return exp.isPlaying || explosion.isPlaying || slow.isPlaying || fast.isPlaying; });
			Destroy(gameObject);
		}
	}
}