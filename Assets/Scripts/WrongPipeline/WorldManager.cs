using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;

namespace WrongPipeline
{
	public class WorldManager : MonoBehaviour
	{
		public static WorldManager Instance;
		public static float persBoxes = 0;
		public static float orthoBoxes = 0;

		public static Material PerspectiveMaterial { get { return Instance ? Instance.perspectiveMaterial : null; } }
		public static Material OrthoMaterial { get { return Instance ? Instance.orthoMaterial : null; } }
		public static Material PlanetMaterial { get { return Instance ? Instance.planetMaterial : null; } }

		public Player player;
		public PlayerCamera playerCamera;
		public GameObject[] planets;
		public int minPlanets = 1;
		public int maxPlanets = 7;
		public float minScale = 0.5F;
		public float maxScale = 5.0F;
		public float maxBlend = 0.95F;
		public Rect spawnArea;
		public Material perspectiveMaterial;
		public Material orthoMaterial;
		public Material planetMaterial;
		public Image perspImage;
		public Image orthoImage;
		public float smoothTime = 0.4F;
		public GameObject mainCamera;
		public GameObject ingameUI;
		public GameObject mainUI;
		public GameObject text1;
		public GameObject text2;
		public GameObject text3;
		public GameObject text4;
		public GameObject background;

		private List<Planet> spawnedPlanets = new List<Planet>();
		private ProjectionBlender blender;
		private bool skipEnabled = false;

		void Awake ()
		{
			Instance = this;
			blender = playerCamera.GetComponent<ProjectionBlender>();
		}

		IEnumerator Start ()
		{
			int planetsCount = Random.Range(minPlanets, maxPlanets + 1);
			persBoxes = 0;
			orthoBoxes = 0;

			for (int i = 0; i < planetsCount; i++)
			{
				Planet planet = Instantiate<GameObject>(planets[Random.Range(0, planets.Length)], transform).GetComponent<Planet>();
				Vector3 position;
				Quaternion rotation;
				float scale = Random.Range(minScale, maxScale);
				float radius = planet.GetFieldRadius(scale);
				bool spawned = true;

				if (i == 0)
				{
					position = Vector3.zero;
					rotation = Random.rotation;
				}
				else
				{
					int tries = 10;

					do
					{
						Planet reference = spawnedPlanets[Random.Range(0, spawnedPlanets.Count)];
						rotation = Random.rotation;
						position = reference.transform.position + rotation * (Vector3.up * (radius + reference.GetFieldRadius()));
						spawned = true;

						foreach (Planet other in spawnedPlanets)
							if (other != reference && (position - other.transform.position).magnitude < (other.GetFieldRadius() + radius))
								spawned = false;
					}
					while (!spawned && --tries > 0);
				}

				if (spawned)
				{
					spawnedPlanets.Add(planet);	
					planet.transform.position = Vector3.one * 10000;
					yield return null;
					yield return planet.Create(scale);
					planet.transform.position = position;
					planet.transform.rotation = rotation;
				}
				else
					Destroy(planet.gameObject);
			}

			Planet spawnPlanet = spawnedPlanets[Random.Range(0, spawnedPlanets.Count)];
			Quaternion spawnRotation = Random.rotation;
			Vector3 spawn = spawnRotation * (Vector3.up * (spawnPlanet.radius + 200));
			player.transform.position = spawnPlanet.transform.position + spawn;
			player.transform.rotation = spawnRotation;
			playerCamera.SetOnPlayer();
			player.currentPlanet = spawnPlanet;

			ingameUI.SetActive(false);
			mainUI.SetActive(true);

			UpdatePlayerCameraProjection();
			blender.UpdateMatrices();
			StartCoroutine(C_Start());
		}

		private IEnumerator C_WaitSkip ()
		{
			yield return new WaitForSeconds(2);
			skipEnabled = true;
		}

		public void StartGame ()
		{
			ingameUI.SetActive(true);
			mainUI.SetActive(false);
			playerCamera.gameObject.SetActive(true);
			mainCamera.SetActive(false);
			player.enabled = true;
		}



		private IEnumerator C_FadeInOut (GameObject o, float start, float end, float delay = 2.0F, bool destroy = false)
		{
			float time = 0.0F;
			AnimationCurve curve = AnimationCurve.EaseInOut(0, start, delay, end);

			while (time <= delay)
			{
				if (o != background && skipEnabled && Input.GetKeyDown(KeyCode.Space))
				{
					skipEnabled = false;
					StopAllCoroutines();
					text1.SetActive(false);
					text2.SetActive(false);
					text3.SetActive(false);
					text4.SetActive(false);
					mainUI.SetActive(false);
					skippedIntro = true;
					// StartCoroutine(C_FadeInOut(background, 1, 0, 4.0F, true));
					StartGame();
					yield break;
				}
				
				float alpha = curve.Evaluate(time);

				Text[] texts = o.GetComponentsInChildren<Text>();
				Image[] images = o.GetComponentsInChildren<Image>();

				foreach (Text text in texts)
					text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
				foreach (Image text in images)
					text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);

				time += Time.deltaTime;
				yield return null;
			}

			if (destroy)
				o.SetActive(false);
		}

		private bool skippedIntro = false;

		private IEnumerator C_Start ()
		{
			while (true)
			{
				if (Input.GetKeyDown(KeyCode.Space))
				{
					StartCoroutine(C_WaitSkip());
					GetComponent<AudioSource>().Play();
					yield return C_FadeInOut(mainUI, 1.0F, 0.0F);

					mainUI.SetActive(false);
					text1.SetActive(true);
					yield return C_FadeInOut(text1, 0.0F, 1.0F);
					yield return new WaitForSeconds(2.5F);
					yield return C_FadeInOut(text1, 1.0F, 0.0F);

					text1.SetActive(false);
					text2.SetActive(true);
					yield return C_FadeInOut(text2, 0.0F, 1.0F);
					yield return new WaitForSeconds(3F);
					yield return C_FadeInOut(text2, 1.0F, 0.0F);

					text2.SetActive(false);
					text3.SetActive(true);
					yield return C_FadeInOut(text3, 0.0F, 1.0F);
					yield return new WaitForSeconds(2.5F);
					yield return C_FadeInOut(text3, 1.0F, 0.0F);

					text3.SetActive(false);
					text4.SetActive(true);
					yield return C_FadeInOut(text4, 0.0F, 1.0F);
					yield return new WaitForSeconds(2.5F);
					StartGame();
					StartCoroutine(C_UI());
					background.SetActive(false);
					yield return C_FadeInOut(text4, 1.0F, 0.0F, 4.0F);
					text4.SetActive(false);
					yield break;
				}

				yield return null;
			}
		}
		void Update ()
		{
			UpdatePlayerCameraProjection();

			if (skippedIntro)
			{
				StartCoroutine(C_FadeInOut(background, 1, 0, 4.0F, true));
				StartCoroutine(C_UI());
				skippedIntro = false;
			}
		}

		public void UpdatePlayerCameraProjection ()
		{
			if (persBoxes + orthoBoxes > 0)
			{
				if (persBoxes < orthoBoxes)
					blender.blend = orthoBoxes / (persBoxes + orthoBoxes);
				else
					blender.blend = 1.0F - (persBoxes / (persBoxes + orthoBoxes));

				blender.blend = Mathf.Clamp(blender.blend, 0, maxBlend);
			}
		}

		private IEnumerator C_UI ()
		{
			// float speed0 = 0;
			// float speed1 = 0;
			// float current0 = 0;
			// float current1 = 0;
			float target0;
			float target1;

			while (true)
			{
				target0 = Mathf.Clamp01(1.0F - ((blender.currentBlend / maxBlend) * 2.0F)) * 195.0F;
				target1 = Mathf.Clamp01(((blender.currentBlend / maxBlend) - 0.5F) * 2.0F) * 195.0F;

				Vector2 size0 = perspImage.rectTransform.sizeDelta;
				Vector3 size1 = orthoImage.rectTransform.sizeDelta;
				size0.y = target0;
				size1.y = target1;

				perspImage.rectTransform.sizeDelta = size0;
				orthoImage.rectTransform.sizeDelta = size1;

				yield return null;
			}
		}
	}
}