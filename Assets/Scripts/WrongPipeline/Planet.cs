using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace WrongPipeline
{
	[RequireComponent(typeof(SphereCollider))]
	public class Planet : MonoBehaviour
	{
		public GameObject[] props;
		public GameObject[] boxes;
		public float radius = 0.5F;
		public int minProps = 5;
		public int maxProps = 20;
		public int minBoxes = 4;
		public int maxBoxes = 8;
		public float fieldHeight = 10.0F;

		private SphereCollider field;
		private List<Prop> spawnedProps = new List<Prop>();
		private List<Box> spawnedBoxes = new List<Box>();

		public float GetFieldRadius (float scale)
		{
			return (radius * scale) + fieldHeight;
		}

		public float GetFieldRadius ()
		{
			return radius + fieldHeight;
		}

		public IEnumerator Create (float scale)
		{
			transform.localScale *= scale;
			this.radius *= scale;

			int propsCount = Mathf.CeilToInt(Random.Range(minProps * scale, maxProps *scale));
			int propsLayer = LayerMask.NameToLayer("Props");
			gameObject.layer = LayerMask.NameToLayer("PlanetFields");
			GetComponent<MeshRenderer>().sharedMaterial = WorldManager.PlanetMaterial;
			field = GetComponent<SphereCollider>();
			field.radius = (this.radius + fieldHeight) / transform.lossyScale.x;
			field.enabled = false;

			List<Bounds> spawnedBounds = new List<Bounds>();
			Vector3 radius = Vector3.up * (this.radius + 20.0F);
			
			foreach (Prop prop in spawnedProps)
				Destroy(prop.gameObject);
			foreach (Box box in spawnedBoxes)
				Destroy(box.gameObject);

			spawnedProps.Clear();
			spawnedBoxes.Clear();
			yield return null;

			for (int i = 0; i < propsCount; i++)
			{
				GameObject prop = Instantiate<GameObject>(props[Random.Range(0, props.Length)]);
				prop.GetComponent<MeshRenderer>().sharedMaterial = WorldManager.PlanetMaterial;
				Collider collider = prop.GetComponent<Collider>();
				Prop propScript = prop.GetComponent<Prop>();
				prop.transform.localScale *= Random.Range(propScript.minScale * scale, propScript.maxScale * scale);
				prop.layer = propsLayer;
				Vector3 rotatedRadius;
				bool spawned = false;
				int tries = 20;

				do
				{
					Quaternion rotation = Random.rotation;
					rotatedRadius = rotation * radius;
					RaycastHit hit;

					if (Physics.Raycast(transform.position + rotatedRadius, -rotatedRadius, out hit, float.MaxValue) && hit.collider.GetComponent<Prop>() == null && Vector3.Angle(rotatedRadius, hit.normal) < 20)
					{
						// prop.transform.position = hit.point - hit.normal.normalized * propScript.offset * transform.lossyScale.x;
						// prop.transform.rotation = Quaternion.FromToRotation(rotation * Vector3.up, hit.normal) * rotation;
						prop.transform.rotation = rotation;
						prop.transform.position = hit.point - rotatedRadius.normalized * propScript.offset * scale;
						Bounds propBounds = collider.bounds;

						foreach (Bounds bounds in spawnedBounds)
							if (propBounds.Intersects(bounds))
								continue;
						
						prop.transform.parent = transform;
						spawnedBounds.Add(propBounds);
						spawnedProps.Add(propScript);
						// collider.enabled = false;
						spawned = true;
					}
				}
				while (!spawned && --tries > 0);

				if (!spawned)
					Destroy(prop);
			}

///////////////////////// BOXES
			propsCount = Mathf.CeilToInt(Random.Range(minBoxes * scale, minBoxes * scale));

			for (int i = 0; i < propsCount; i++)
			{
				GameObject prop = Instantiate<GameObject>(boxes[Random.Range(0, boxes.Length)]);
				bool ortho = Random.value - 0.5F < 0;
				prop.GetComponentInChildren<MeshRenderer>().sharedMaterial =  ortho ? WorldManager.OrthoMaterial : WorldManager.PerspectiveMaterial;
				Collider collider = prop.GetComponentInChildren<Collider>();
				Box propScript = prop.GetComponent<Box>();
				propScript.isOrtho = ortho;
				prop.transform.localScale *= Random.Range(propScript.minScale, propScript.maxScale);
				prop.layer = propsLayer;
				Vector3 rotatedRadius;
				bool spawned = false;
				int tries = 20;

				

				do
				{
					Quaternion rotation = Random.rotation;
					rotatedRadius = rotation * radius;
					RaycastHit hit;

					if (Physics.Raycast(transform.position + rotatedRadius, -rotatedRadius, out hit, float.MaxValue) && hit.collider.GetComponent<Prop>() == null && Vector3.Angle(rotatedRadius, hit.normal) < 20)
					{
						// prop.transform.position = hit.point - hit.normal.normalized * propScript.offset * transform.lossyScale.x;
						// prop.transform.rotation = Quaternion.FromToRotation(rotation * Vector3.up, hit.normal) * rotation;
						prop.transform.rotation = rotation;
						prop.transform.position = hit.point - rotatedRadius.normalized * propScript.offset * scale;
						Bounds propBounds = collider.bounds;

						foreach (Bounds bounds in spawnedBounds)
							if (propBounds.Intersects(bounds))
								continue;
						
						prop.transform.parent = transform;
						spawnedBounds.Add(propBounds);
						spawnedBoxes.Add(propScript);
						
						// collider.enabled = false;
						spawned = true;
					}
				}
				while (!spawned && --tries > 0);

				if (!spawned)
					Destroy(prop);
				// else
				// {
				// 	if (ortho) WorldManager.orthoBoxes++;
				// 		WorldManager.persBoxes++;
				// }
			}

			field.enabled = true;

			Box[] asdf = GetComponentsInChildren<Box>();
			foreach (Box box in asdf)
				if (box.isOrtho)
					WorldManager.orthoBoxes++;
				else
					WorldManager.persBoxes++;
		}

		private void OnDrawGizmos ()
		{
			Color color = Color.cyan;
			color.a = 0.5F;
			Gizmos.color = color;
			Gizmos.DrawSphere(transform.position, radius);
		}
	}
}