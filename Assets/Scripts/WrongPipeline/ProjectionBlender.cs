﻿using UnityEngine;
using System.Collections;
using KUtils;

namespace WrongPipeline
{
	[DisallowMultipleComponent]
	[RequireComponent (typeof(Camera))]
	public class ProjectionBlender : MonoBehaviour
	{
		[Range(0.0F, 1.0F)]
		public float blend = 0.0F;
		[Range(1.0F, 179.0F)]
		public float fov = 80.0F;
		public float orthographicSize = 5.0F;
		public float near = 0.3F;
		public float far = 1000.0F;
		public float smoothTime = 0.5F;
		public bool disable = false;

		private Matrix4x4 ortho, perspective;
		private new Camera camera;
		private float aspect;
		[HideInInspector]
		public float currentBlend;

		// private void OnValidate ()
		// {
		// 	camera = GetComponent<Camera>();
		// 	currentBlend = blend;

		// 	if (disable)
		// 		camera.ResetProjectionMatrix();
		// 	else
		// 		UpdateMatrices();
		// }

		private void Awake ()
		{
			camera = GetComponent<Camera>();
			currentBlend = blend;
		}

		void Start()
		{
			currentBlend = blend;
			UpdateMatrices();
		}

		public void UpdateMatrices ()
		{
			if (camera == null)
				camera = GetComponent<Camera>();

			aspect = (float) camera.pixelWidth / (float) camera.pixelHeight;

			ortho = Matrix4x4.Ortho
			(
				-orthographicSize * aspect, orthographicSize *  aspect,
				-orthographicSize, orthographicSize,
				near, far
			);

			perspective = Matrix4x4.Perspective(fov, aspect, near, far);
			camera.projectionMatrix = KMathf.MatrixLerp(perspective, ortho, currentBlend);

			Matrix4x4 customOrtho = GL.GetGPUProjectionMatrix(ortho, false);
			float scale = Mathf.Lerp(6.5F, 1.0F, currentBlend);
			
			// customOrtho.m00 *= scale; customOrtho.m11 *= scale; customOrtho.m22 *= scale;
			customOrtho.m00 *= scale; customOrtho.m11 *= scale; customOrtho.m22 *= scale;
			SetMatrix(WorldManager.OrthoMaterial, customOrtho);

			Matrix4x4 customPerspective = GL.GetGPUProjectionMatrix(perspective, false).transpose;
			scale = Mathf.Lerp(1.5F, 0.2F, currentBlend);

			customPerspective.m00 *= scale; customPerspective.m11 *= scale; customPerspective.m22 *= scale;
			// customPerspective.m00 *= -scale; customPerspective.m11 *= scale; customPerspective.m22 *= -scale;
			SetMatrix(WorldManager.PerspectiveMaterial, customPerspective);
		}

		private static void SetMatrix (Material material, Matrix4x4 matrix)
		{
			material.SetFloat("custom_m00", matrix.m00);
			material.SetFloat("custom_m01", matrix.m01);
			material.SetFloat("custom_m02", matrix.m02);
			material.SetFloat("custom_m03", matrix.m03);

			material.SetFloat("custom_m10", matrix.m10);
			material.SetFloat("custom_m11", matrix.m11);
			material.SetFloat("custom_m12", matrix.m12);
			material.SetFloat("custom_m13", matrix.m13);

			material.SetFloat("custom_m20", matrix.m20);
			material.SetFloat("custom_m21", matrix.m21);
			material.SetFloat("custom_m22", matrix.m22);
			material.SetFloat("custom_m23", matrix.m23);

			material.SetFloat("custom_m30", matrix.m30);
			material.SetFloat("custom_m31", matrix.m31);
			material.SetFloat("custom_m32", matrix.m32);
			material.SetFloat("custom_m33", matrix.m33);
		}

		private float speed = 0;
		private float lastBlend;

		void Update ()
		{
			if (!disable) 
			{
				lastBlend = currentBlend;
				currentBlend = Mathf.SmoothDamp(currentBlend, blend, ref speed, smoothTime);

				if (Mathf.Abs(currentBlend - blend) < 0.0005F)
					currentBlend = blend;

				if (currentBlend != lastBlend)
					UpdateMatrices();
			}
		}
	}
}