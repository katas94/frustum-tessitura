using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Test))]
public class TestEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		Test test = target as Test;

		DrawDefaultInspector();

		if (GUILayout.Button("Test"))
			test.GetComponent<ParticleSystem>().Play();

		Camera camera = test.GetComponent<Camera>();

		if (camera)
		{
			Matrix4x4 mat = GL.GetGPUProjectionMatrix(camera.projectionMatrix, false);
			string matrix = "";

			for (int i = 0; i < 16; i++)
			{
				if (i % 4 == 0)
					matrix += "\n";

				matrix += mat[i].ToString("00.0000");

				if (i < 15)
					matrix += ",\t";
				
			}

			EditorGUILayout.LabelField("Projection");
			EditorGUILayout.TextArea(matrix);

			mat = camera.worldToCameraMatrix;
			matrix = "";

			for (int i = 0; i < 16; i++)
			{
				if (i % 4 == 0)
					matrix += "\n";

				matrix += mat[i].ToString("00.0000");

				if (i < 15)
					matrix += ",\t";
				
			}

			EditorGUILayout.LabelField("View");
			EditorGUILayout.TextArea(matrix);

			mat = Matrix4x4.Ortho
			(
				-camera.orthographicSize * camera.aspect, camera.orthographicSize *  camera.aspect,
				-camera.orthographicSize, camera.orthographicSize,
				camera.nearClipPlane, camera.farClipPlane
			);

			mat = GL.GetGPUProjectionMatrix(mat, false);

			matrix = "";

			for (int i = 0; i < 16; i++)
			{
				if (i % 4 == 0)
					matrix += "\n";

				matrix += mat[i].ToString("00.0000");

				if (i < 15)
					matrix += ",\t";
				
			}

			EditorGUILayout.LabelField("custom ortho");
			EditorGUILayout.TextArea(matrix);
		}
	}
}