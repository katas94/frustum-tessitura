using UnityEngine;
using System.Collections.Generic;

namespace WrongPipeline
{
	public class Prop : MonoBehaviour
	{
		public float minScale = 1.0F;
		public float maxScale = 1.0F;
		public float offset = 0.08F;
	}
}