using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace WrongPipeline
{
	[RequireComponent(typeof(LineRenderer))]
	public class Player : MonoBehaviour
	{
		public new PlayerCamera camera;
		public float gravityConstant = 60.0F;
		public float jumpSpeed = 25;
		public float walkSpeed = 10;
		public float runSpeed = 20;
		public bool gravityEnabled = true;
		public GameObject dot;
		public float shootDistance = 20.0F;

		[Header("Keep up")]
		public float  maxSmoothTime = 0.5F;
		public float maxDistance = 10.0F;

		[Header("Ground check")]
		public float groundCheckHeight = 0.5F;
		public float groundCheckMargin = 0.05F;

		[HideInInspector]
		public Rigidbody body;
		[HideInInspector]
		public Planet currentPlanet;
		private CapsuleCollider capsule;
		private int planetFieldsLayer;
		private int playerLayer;

		private LineRenderer line;
		private Vector3 gravity, velocity, distance, currentUp;
		private bool grounded = false;
		private bool doubleJump =  false;

		private void Awake ()
		{
			body = GetComponent<Rigidbody>();
			capsule = GetComponent<CapsuleCollider>();
			planetFieldsLayer = LayerMask.NameToLayer("PlanetFields");
			playerLayer = LayerMask.NameToLayer("Player");
			line = GetComponent<LineRenderer>();
			line.enabled = false;
			line.positionCount = 2;
			line.useWorldSpace = false;
			line.SetPosition(0, Vector3.zero);
		}

		private void Start ()
		{
			grounded = false;
			doubleJump = true;
			body.velocity = -transform.up * 40;
			StartCoroutine(C_KeepUp());
		}

		private void FixedUpdate ()
		{
			Quaternion rotation = Quaternion.FromToRotation(transform.up, currentUp) * transform.rotation;
			body.MoveRotation(rotation);
		}

		private void Update ()
		{
			velocity.x = 0.0F;
			velocity.y = transform.InverseTransformVector(body.velocity).y;
			velocity.z = 0.0F;

			float speed = Input.GetKey(KeyCode.LeftShift) ? runSpeed : walkSpeed;

			if (Input.GetKey(KeyCode.A))
				velocity += Vector3.left * speed;
			if (Input.GetKey(KeyCode.D))
				velocity += Vector3.right * speed;
			if (Input.GetKey(KeyCode.W))
				velocity += Vector3.forward * speed;
			if (Input.GetKey(KeyCode.S))
				velocity += Vector3.back * speed;
			
			if ((grounded || !doubleJump) && Input.GetKeyDown(KeyCode.Space))
			{
				if (grounded)
					grounded = false;
				else
					doubleJump = true;
				
				velocity.y = jumpSpeed;
				GetComponent<AudioSource>().Play();
			}

			// gravedad
			gravity = Vector3.zero;

			if (currentPlanet)
			{
				distance = currentPlanet.transform.position - transform.position;
				float dst = distance.magnitude - currentPlanet.radius;
				if ((dst *= dst * 0.001F) < 1) dst = 1;
				gravity += distance.normalized * (gravityConstant / dst);
			}

			velocity.y -= gravity.magnitude * Time.deltaTime;
			// CheckGround();
			body.velocity = transform.TransformVector(velocity);

			RaycastHit hit;
			line.enabled = false;
			dot.SetActive(false);

			if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, float.MaxValue, ~(planetFieldsLayer & playerLayer)))
			{
				Box box = hit.collider.GetComponentInParent<Box>();

				if (box && Physics.Raycast(transform.position, hit.point - transform.position, out hit, shootDistance, ~(planetFieldsLayer & playerLayer)))
				{
					box = hit.collider.GetComponentInParent<Box>();

					if (box)
					{
						line.enabled = true;
						line.SetPosition(1, transform.InverseTransformPoint(hit.point));
						dot.SetActive(true);
						dot.transform.position = hit.point;

						if (Input.GetMouseButtonDown(0))
							box.Fire();
					}
				}
			}
			
		}

		private void CheckGround ()
		{
			RaycastHit hit;

			Vector3 point0 = (0.5F + groundCheckHeight) * transform.up + transform.position;
			Vector3 point1 = point0 - transform.up;
			float minDistance = groundCheckHeight + groundCheckMargin;

			if (Physics.CapsuleCast(point0, point1, capsule.radius, -transform.up, out hit) && !hit.collider.isTrigger
				&& hit.distance < minDistance
				&& Vector3.Angle(hit.normal, transform.up) < 60)
			{
				transform.position += transform.up * (minDistance - hit.distance);

				if (velocity.y < 0)
					velocity.y = 0.0F;
			}
		}

		private void OnTriggerEnter (Collider other)
		{
			if (other.gameObject.layer == planetFieldsLayer)
			{
				Planet planet = other.GetComponent<Planet>();

				if (planet)
				{
					if (planet != currentPlanet)
						camera.PlanetChanged();
					
					currentPlanet = planet;
				}
			}
		}

		private void OnCollisionEnter (Collision collisionInfo)
		{
			if (Vector3.Angle(transform.up, collisionInfo.contacts[0].normal) < 60)
			{
				grounded = true;
				doubleJump = false;
			}
		}

		private IEnumerator C_KeepUp ()
		{
			Vector3 currentVelocity = Vector3.zero;
			currentUp = transform.up;

			while (true)
			{
				if (currentPlanet)
				{
					Vector3 distance = currentPlanet.transform.position - transform.position;
					float dst = distance.magnitude - currentPlanet.radius - 0.5F;
					if (dst < 0) dst = 0;
					currentUp = Vector3.SmoothDamp(currentUp, -gravity.normalized, ref currentVelocity, Mathf.Clamp(maxSmoothTime * (dst / maxDistance), 0, maxSmoothTime));
				}

				yield return null;
			}
		}
	}
}