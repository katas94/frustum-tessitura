﻿Shader "kataS/Custom Projection/Directional"
{
    Properties
    {
		_Color ("Color", Color) = (1,1,1,1)
        [NoScaleOffset] _MainTex ("Texture", 2D) = "white" {}
		[Toggle(CUSTOM_PROJECTION_ON)] _CustomProjection ("Enable custom pojection", Int) = 0
		
    }
    SubShader
    {
        Pass
        {
			Cull Off
            Tags {"LightMode"="ForwardBase"}
            CGPROGRAM
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
			#include "CustomProjection.cginc"
            #pragma vertex vert
            #pragma fragment frag
			#pragma shader_feature CUSTOM_PROJECTION_ON
			

            // compile shader into multiple variants, with and without shadows
            // (we don't care about any lightmaps yet, so skip these variants)
            #pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
            // shadow helper functions and macros
            #include "AutoLight.cginc"

			uniform half4 _Color;

            struct v2f
            {
                float2 uv : TEXCOORD0;
                SHADOW_COORDS(1) // put shadows data into TEXCOORD1
                fixed3 diff : COLOR0;
                fixed3 ambient : COLOR1;
                float4 pos : SV_POSITION;
                float4 color : COLOR2;
            };
            v2f vert (appdata_full v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = v.texcoord;
                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0.rgb;
                o.ambient = ShadeSH9(half4(worldNormal,1));
				o.color = v.color;
                // compute shadows data
                TRANSFER_SHADOW(o)

				float z = o.pos.z;
				CUSTOM_PROJECTION(v.vertex, o.pos)
				o.pos.z = z;

                return o;
            }

            sampler2D _MainTex;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv) * i.color * _Color;
                // compute shadow attenuation (1.0 = fully lit, 0.0 = fully shadowed)
                fixed shadow = SHADOW_ATTENUATION(i);
                // darken light's illumination with shadow, keep ambient intact
                fixed3 lighting = i.diff * shadow + i.ambient;
                col.rgb *= lighting;
                return col;
            }
            ENDCG
        }

        // UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
    }
}