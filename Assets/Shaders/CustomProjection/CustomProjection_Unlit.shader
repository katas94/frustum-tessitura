﻿Shader "kataS/Custom Projection/Unlit"
{
    Properties
	{
		_MainTex ( "Main Texture", 2D ) = "white" {}
        _Tint ("Tint", Color) = (1, 1, 1, 1)
		[Toggle(CUSTOM_PROJECTION_ON)] _CustomProjection ("Enable custom pojection", Int) = 0
    }

    SubShader
	{
		Cull Back
		LOD 100

		Tags
		{
			"Queue" = "Geometry+0"
			"RenderType" = "Opaque"
			"PreviewType" = "Sphere"
		}

		Pass
		{
			CGPROGRAM
				#include "UnityCG.cginc"
				#include "CustomProjection.cginc"
				#pragma vertex Vertex
				#pragma fragment Fragment
				#pragma shader_feature CUSTOM_PROJECTION_ON

				uniform sampler2D	_MainTex;
     			uniform float4		_MainTex_ST;
				uniform half4		_Tint;

				struct VertexInput
				{
					float4	vertex		: POSITION;
					half2	uv0			: TEXCOORD0;
					half4	color		: COLOR;
				};

				struct FragmentInput
				{
					float4	position	: SV_POSITION;
					half2	uv0			: TEXCOORD0;
					half4	color		: COLOR;
				};

				FragmentInput Vertex (VertexInput input)
				{
					FragmentInput output;

					#ifdef CUSTOM_PROJECTION_ON
						output.position = CustomObjectToClipPos(input.vertex);
					#else
						output.position = UnityObjectToClipPos(input.vertex);
					#endif

					output.uv0 = TRANSFORM_TEX(input.uv0, _MainTex);
					output.color = input.color;

					return output;
				}

				half4 Fragment (FragmentInput input) : SV_TARGET
				{
					return tex2D(_MainTex, input.uv0) * _Tint * input.color;
				}
			ENDCG
		}
	}
}