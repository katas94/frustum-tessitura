﻿Shader "kataS/Custom Projection/Pass"
{
    Properties
	{
		_MainTex ( "Main Texture", 2D ) = "white" {}
        _Tint ("Tint", Color) = (1, 1, 1, 1)
		[Toggle(CUSTOM_PROJECTION_ON)] _CustomProjection ("Enable custom pojection", Int) = 0
    }

    SubShader
	{
		Pass
		{
			Name "CustomProjection"
			Cull Back
			LOD 100

			Tags
			{

				"Queue" = "Geometry+0"
				"RenderType" = "Opaque"
				"PreviewType" = "Sphere"
			}

			Stencil
			{
                Ref 200
                Comp Always
                Pass Replace
            }

			CGPROGRAM
				#define CUSTOM_PROJECTION_ON

				#include "UnityCG.cginc"
				#include "CustomProjection.cginc"
				#pragma vertex Vertex
				#pragma fragment Fragment

				uniform sampler2D	_MainTex;
     			uniform float4		_MainTex_ST;
				uniform half4		_Tint;

				struct VertexInput
				{
					float4	vertex		: POSITION;
				};

				struct FragmentInput
				{
					float4	position	: SV_POSITION;
				};

				FragmentInput Vertex (VertexInput input)
				{
					FragmentInput output;

					output.position = CustomObjectToClipPos(input.vertex);

					return output;
				}

				half4 Fragment (FragmentInput input) : SV_TARGET
				{
					return float4(0, 0, 0, 0);
				}
			ENDCG
		}
	}
}