#ifdef CUSTOM_PROJECTION_ON
	#include "UnityCG.cginc"
	#define CUSTOM_PROJECTION(vertex, output) output = CustomObjectToClipPos(vertex);

	float	custom_m00, custom_m01, custom_m02, custom_m03,
			custom_m10, custom_m11, custom_m12, custom_m13,
			custom_m20, custom_m21, custom_m22, custom_m23,
			custom_m30, custom_m31, custom_m32, custom_m33;

	inline float4 CustomObjectToClipPos (in float3 pos)
	{
		float4x4 custom_Projection = float4x4
		(
			custom_m00, custom_m01, custom_m02, custom_m03,
			custom_m10, custom_m11, custom_m12, custom_m13,
			custom_m20, custom_m21, custom_m22, custom_m23,
			custom_m30, custom_m31, custom_m32, custom_m33
		);

		float4x4 custom_MatrixMVP = mul(custom_Projection, UNITY_MATRIX_MV);
		float4 custom_OrthoOffset = UnityObjectToClipPos(float4(0.0, 0.0, 0.0, 1.0)) - mul(custom_MatrixMVP, float4(0.0, 0.0, 0.0, 1.0));
		// return UnityObjectToClipPos(pos);
		// return mul(custom_MatrixMVP, float4(pos, 1.0));
		return mul(custom_MatrixMVP, float4(pos, 1.0)) + custom_OrthoOffset;
	}

	inline float4 CustomObjectToClipPos (float4 pos)
	{
		return CustomObjectToClipPos(pos.xyz);
	}
#else
	#define CUSTOM_PROJECTION(vertex, output)
#endif