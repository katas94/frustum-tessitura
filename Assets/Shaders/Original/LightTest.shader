﻿Shader "kataS/Custom Projection/Per Vertex PBRwerqre"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}

	SubShader
	{
		Cull Back
		LOD 200

		Tags
		{
			"Queue" = "Geometry+0"
			"RenderType" = "Opaque"
			"PreviewType" = "Sphere"
		}

		CGPROGRAM
			#pragma surface surf Standard fullforwardshadows
			#pragma target 3.0

			struct Input
			{
				float2 uv_MainTex;
			};

			fixed4 _Color;
			half _Glossiness;
			half _Metallic;

			UNITY_INSTANCING_BUFFER_START(Props)
			UNITY_INSTANCING_BUFFER_END(Props)

			void surf (Input IN, inout SurfaceOutputStandard o)
			{
				o.Albedo = _Color.rgb;
				o.Metallic = _Metallic;
				o.Smoothness = _Glossiness;
				o.Alpha = _Color.a;
			}
		ENDCG
	}

	FallBack "Diffuse"
}
